# Notebook Instructions

1. If you are new to Jupyter notebooks, please go through this introductory manual <a href='https://quantra.quantinsti.com/quantra-notebook' target="_blank">here</a>.
1. Any changes made in this notebook would be lost after you close the browser window. **You can download the notebook to save your work on your PC.**
1. Before running this notebook on your local PC:<br>
i.  You need to set up a Python environment and the relevant packages on your local PC. To do so, go through the section on "**Python Installation**" in the course.<br>
ii. You need to **download the zip file available in the last unit** of this course. The zip file contains the data files and/or python modules that might be required to run this notebook.

## Let us begin

It is now time to understand how to code programs in Python. Python uses a simple syntax which makes it very easy for someone learning to program for the first time. This notebook is comprehensively designed to help you get familiarized with programming and learn the basics of Python.

## What is programming?

Programming is the way of telling a machine what to do. This machine might be your computer, smartphone, or tablet. The task might be something as simple as noting down today’s date or capturing information about the Earth’s atmosphere on a satellite. Programming has a lot of alias names and they’re used interchangeably. It goes by programming, developing, or coding all of which involves creating software that gets a machine to do what you want it to do.

### Hello World program

How would you make Python print "Hello World" for you? Well, it's never been this easy, just use the <b> print </b> command.


```python
print("Hello World!")
```

    Hello World!



```python
# You may try other variations
print("I am new to programming!")
print("Python is cool!")
```

    I am new to programming!
    Python is cool!


## Introduction to Python programming
Python places more weight on coding productivity and code readability. Python
makes use of simple syntax which looks like written English. It talks with words and
sentences, rather than characters. Python is a portable language. Python can be installed
and run on any computer.

Python coding is a lot of fun and is easy. Take this Python code for an example:



```python
x = 2
y = 5
sum = x + y
print(sum)
```

    7


Even without any coding background, you can easily make out that the code adds up two numbers and prints it. You may modify the code above and try different mathematical operations on different variables.

## Variables, data Types and objects

We have studied how to use a variable in python in the previous video unit.


```python
x = 100
```

One thing to keep in mind, the equal '=' sign used while assigning a value to a variable. It should not be read as 'equal to'. It should be read or interpreted as "is set to". 

In the previous example, we will read that the value of variable 'x' <b>is set to</b> '100'.


```python
y = 50  # Initialising a new variable 'y' whose value is set to 50
```

### ID of an object
The keyword <b>id ()</b> specifies the object's address in memory. Look at the code below for seeing the addresses of different objects.


```python
id(x)
```




    140720750243312



You may change the variable name inside the function id() to print the id's of other variables.


```python
id(y)
```




    140720750241712



Note : The IDs of 'x' and 'y' are different.

### Data type of an object

The type of an object cannot change. It specifies two things, the operations that are allowed and the set of values that the object can hold. The keyword type() is used to check the type of an object.


```python
type(x)
```




    int




```python
type(y)
```




    int



Now, let us try something more.


```python
x = x + 1.11
print(x)      # This will print the new value of 'x' variable
type(x)        # This will print the most updated data type of 'x'
```

    101.11





    float



Now you may check the ID of the new 'x' object which is now a float and not an integer.


```python
id(x)
```




    1818280269904



Note this is different from the 'int x' ID. 

Python automatically takes care of the physical representation of different data types i.e. an integer value will be stored in a different memory location than a float or string.


```python
# let us now convert variable 'x' to a string data type and observe the changes
x = "hundred"
print(x)
type(x)
```

    hundred





    str




```python
id(x)
```




    1818280371312



### Object references

Let us observe the following code.


```python
a = 123
b = a
```

Where will the object point? Will it be to the same object ID?


```python
id(a)
```




    140720750244048




```python
id(b)
```




    140720750244048



Yes, Since same value is stored in both the variables 'a' and 'b', they will point to the same memory location or in other words, they will have the <b>same object ID</b>.

## Multi-line statements

There is no semicolon to indicate an end of the statement and therefore Python interprets the end of the line as the end of the statement.

For example, a code to calculate total marks.


```python
biology_marks = 82
physics_marks = 91
maths_marks = 96
chemistry_marks = 88
total_marks = biology_marks + physics_marks + maths_marks + chemistry_marks
print (total_marks)
```

    357


However, if a line is too long, the code can be made readable by adding a split, to a single line of code and convert them into multiple lines. In such scenarios, use backward slash as line continuation character to specify that the line should continue.


```python
total_marks = biology_marks + \
    physics_marks + \
    maths_marks + \
    chemistry_marks

print(total_marks)
```

    357


##  Indentation

Python forces you to follow proper indentation. The number of spaces in indentation can be different, but all lines of code within the same block should have the same number of spaces in the indentation.

For example, the 3rd line of the code in the cell below shows incorrect indentation. Try running the code to see the error that it throws.


```python
# Python Program to calculate the square of number
num = 8
  num_sq = num ** 2
print (num_sq)
```


      File "/tmp/ipykernel_23/2481634260.py", line 3
        num_sq = num ** 2
        ^
    IndentationError: unexpected indent




```python
# On removing the indent
num = 8
num_sq = num ** 2
print(num_sq)
```

    64


In the upcoming IPython notebook, 
you will learn about <b>Modules and Packages</b>. But before that, let us print a statement in Python. <br><br>
