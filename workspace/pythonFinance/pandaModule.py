import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]

stocks = ["PARAS", "Reliance", "BPCL", "Devyani", "VEDL"]

dataFrame = pd.DataFrame(data=np.random.randn(12, 5)*1000, columns=stocks, index=month)

print(dataFrame.head())

# functions in  panda

infy = pd.read_csv('C:/Users/suraj/Documents/infy.csv')
print(infy.head())
print(infy.tail())

# count = This method returns the number of non-null observations(number of rows ) over the requested observations.
print(infy.count())
# count for specific column
print("Print Close Column Count \n", infy["Close Price"].count())

# min() = This method returns the minimum value over the requested observations.
print("Print min value of close column \n", infy["Close Price"].min())
print("Print infy min  \n", infy.min())

# max = This method returns the maximum value over the requested observations.
print("Print max value of close column \n", infy["Close Price"].max())
print("Print infy max  \n", infy.max())


# mean = The mean of a set of observations is the arithmetic average of the values.
print("Print mean value of close column \n", infy["Close Price"].mean())
print("Print infy mean  \n", infy.mean(numeric_only=True))

# median = The median is a statistical measure that determines the middle value of a dataset listed in ascending order.
print("Print median value of close column \n", infy["Close Price"].median())
print("Print infy median  \n", infy.median(numeric_only=True))

# mode = The mode is the value that appears most frequently in a data set
print("Print mode value of close column \n", infy["Close Price"].mode())
print("Print infy modes  \n", infy.mode(dropna=True))

# sum = This method returns the sum of all the values of the requested observations.
print("Print sum value of close column \n", infy["Close Price"].sum())
print("Print infy SUM  \n", infy.sum(numeric_only=True, skipna=True))

# diff = This method returns the 'difference' between the current observation and the previous observation.
print("Print diff value of close column \n", infy["Close Price"].diff())


# var = Variance is a statistical measurement of the spread between numbers in a data set.
print("Print variance of close column \n", infy["Close Price"].var())
# pct_change = This method returns the percentage change of the current observation with the previous observation.
print("Print % change in  value of close column \n", infy["Close Price"].pct_change())

# Deviation = Standard deviation is a statistical measure that measures the dispersion of a dataset relative to its mean.

print("Print standard deviation in  value of close column \n", infy["Close Price"].std())


# percentage change on plot

"""
plt.style.use('seaborn-darkgrid')
plt.figure(figsize=(10, 5))
plt.ylabel("Daily Returns of INFY")
plt.xlabel("Observation Daily")
infy["Close Price"].pct_change().plot()
plt.show()
"""
# DataFrame.rolling(window=).mean()
# A moving average(also known as rolling average) is a calculation used to analyze data points
# by creating a series of averages of different subsets of the full data set.
# It is commonly used as a technical indicator.
"""
print("Print moving average of close column \n", infy["Close Price"].rolling(window=20).mean().plot())
infy["Close Price"].plot()
infy["Close Price"].rolling(window=20).mean().plot()

plt.grid(visible=True)
plt.show()
"""
# DataFrame.expanding(min_periods=).mean()
# This method returns the 'expanding' mean of the requested observations.
# A common alternative to rolling mean is to use an expanding window mean,
# which returns the value of the mean with all the observations avaliable up to that point in time.
print("Expanding mean for 20 days", infy["Close Price"].expanding(min_periods=20).mean())

infy["Close Price"].expanding(min_periods=20).mean().plot()
infy["Close Price"].plot()

plt.show()
