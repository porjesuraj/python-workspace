import numpy as np
import pandas as pd
# to Array
list1 = [2, 3, 4, 5, 5, 6, 767, 78, 8]
npArray = np.array(list1)
print("List ")
print(list1)
print(type(list1))
print("To Array")
print(npArray)
print(type(npArray))

# create dataset

data = np.arange(1, 100, 5, int)
print(data)

# using linspace

data2 = np.linspace(start=1, stop=100, num=7, endpoint=True, retstep=True, dtype=float)
print(data2)
print("Dimensions and datatype")
print("Array data type  = ", npArray.dtype, "Array D type = ", np.ndim(npArray))
scalar = np.array("one element")
print("Array data type  = ", scalar.dtype, "Array D type = ", np.ndim(scalar))

# 1 D Array

array1 = np.array([1, 2, 3, 4, 5, 6])

print("array : ", array1)
print("array Data type : ", array1.dtype)
print("array Dimension : ", np.ndim(array1))

array2 = np.array([[1, 2, 3], [4, 5, 6]])
print("array : ", array2)
print("array Data type : ", array2.dtype)
print("array Dimension : ", np.ndim(array2))


# Student data
student_data = np.array([["Name", "Year", "Marks"],
                        ["ARaj", "2016", 79],
                        ["BRaj", "2016", 89],
                        ["CRaj", "2016", 69],
                        ["DRaj", "2016", 59]])

student_data2 = {"Name": ["Raj", "RajK", "RajD"],
                 "Marks": [70, 50, 60]}
data_frame1 = pd.DataFrame(student_data2)
print("Data frame from Array")
print(data_frame1)
for item in data_frame1.Marks:
    print(item)

print("Average", np.average(data_frame1.Marks))
print("Array dimension", np.ndim(student_data))

# 3D Array

aArray = np.array([[[1, 2,3],[4, 5, 6]],
                    [["A", "B", "C"],["D", "E", "F"]]])
print("Array elements : \n", aArray, "\n Array dimension: ", np.ndim(aArray))

ArrayForShape = np.array([[11, 22, 33],
              [12, 24, 36],
              [13, 26, 39],
              [14, 28, 42],
              [15, 30, 45],
              [16, 32, 48]])


print("Array row and column", ArrayForShape.shape)

ArrayForShape.shape = (9, 2)

print("New Array : \n ", ArrayForShape)
