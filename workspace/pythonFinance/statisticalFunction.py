import pandas as pd
import numpy as np

infy = pd.read_csv("C:/Users/suraj/Documents/infy.csv")
new_infy = infy[:9:]
print(new_infy.shape)
print(new_infy)
# Indexing
# it helps us to identify the exact position of data which is important while analysing data.
# While studying indexing, we will also focus on how to slice and dice the data according to our needs in a dataframe.

# 1. Indexing using .loc()
# It is a 'label-location' based indexer for selection of data points.
print(new_infy.loc[:, ["Close Price", "Open Price"]])

# get 7 row
print(infy.loc[0:6, ["Close Price"]])

# select row and column specified

print(infy.loc[[2, 4, 6, 8], ["Close Price"]] > 700)

# Indexing using .iloc()
# .iloc() is extremely useful when your data is not labelled and,
# you need to refer to columns using their integer location instead
#  Remember that the '.iloc()' method DOES NOT include the rows and columns in its stop argument
print(infy.iloc[2:5, :3])

print(infy.iloc[[1, 2, 3, 4, 5], [1, 2, 3, 4, 5]])

# Missing values are values that are absent from the dataframe.

# DataFrame.isnull()
# This method returns a Boolean result.
# It will return 'True' if the data point has a 'NaN' (Not a Number) value.
# Missing data is represented by a NaN value.

print(new_infy["Close Price"].isnull())

print(new_infy["Close Price"].notnull())

# DataFrame.fillna()
# The .fillna() method will fill all the 'NaN' values with a scalar value of your choice.

print(new_infy["Close Price"].fillna(10))

# If we want to do 'fillna()' using the 'backfill' method,
# then backfill will take the value from the next row and fill the NaN value with that same value
# using the 'ffill' method, then
# ffill will take the value from the previous row and fill the NaN value with that same value
# 'pad' does the same thing as 'ffill'
print("Backfill \n", new_infy['Close Price'].fillna(method='backfill'))
print("ffill \n", new_infy['Close Price'].fillna(method='ffill'))

# DataFrame.dropna()
# This method will drop the entire 'row' or 'column'
# which has even a single 'NaN' value present, as per the request.
#  By default, dropna() will exclude or drop all the rows which have even one NaN value in it
# If we specify the axis = 1, it will exclude or drop all the columns which has even one NaN value in it
print(new_infy.dropna())

# Replacing values
# Replacing helps us to select any data point in the entire dataframe and replace it with the value of our choice.

dataframe = pd.DataFrame({"index": [1, 2, 3, 4],
                          "Percent": [80, 70, 90, 55.5]})
print(dataframe)

print(dataframe.replace({55.5: 100}))

# Reindexing changes the row labels and column labels of a dataframe.
# To reindex means to confirm the data to match a given set of labels along a particular axis.
infy_reindexed = infy.reindex(index=[0, 2, 4, 6, 8], columns=[
                              'Open Price', 'High Price', 'Low Price', 'Close Price'])

print(infy_reindexed)

"""
Pandas 'groupby'
Any groupby operation involves one of the following operations on the original dataframe/object. They are:
1. Splitting the data into groups based on some criteria.
2. Applying a function to each group separately.
3. Combining the results into a single dataframe.

Splitting the data is pretty straight forward. 
What adds value to this split is the 'Apply' step.
 This makes 'Groupby' function interesting.
  In the apply step, you may wish to do one of the following:
a. Aggregation − Computing a summary statistic. Eg: Compute group sums or means.

b. Transformation − performs some group-specific operation. Eg: Standardizing data (computing z-score) within the group.

c. Filtration − discarding the data with some condition.
"""

my_portfolio = {'Sector': ['FOOD', 'FOOD', 'ENERGY', 'CORE', 'CORE',
                           'CORE'],

                'Company':   ['DEVYANI', 'ZOMATO', 'BPCL', 'VEDL', 'BEL',
                              'JYOTISTRUC'],

                'MarketCap': ['Large Cap', 'Mid Cap', 'Large Cap', 'Mid Cap', 'Mid Cap',
                              'Mid Cap'],

                'Share Price': [90, 132, 416.81, 306.75, 203.5, 18.50],

                'Amount Invested': [14850, 6600, 15422, 3067.5, 2035, 1850]}

portfolio = pd.DataFrame(my_portfolio)
print(portfolio)
print(portfolio.groupby('Sector').groups)
print("Groupby M and S \n ", portfolio.groupby(['Sector', 'MarketCap']).groups)

# Iterating through groups

grouped = portfolio.groupby('Sector')

for name, group in grouped:
    print("Name : ", name)
    print("Group \n", group)

# get specific group
print("Specific group for FOOD \n", grouped.get_group('FOOD'))

# Aggregation
groupedByMarketCap = portfolio.groupby('MarketCap')
print("Group by market cap \n", groupedByMarketCap.get_group('Large Cap'))
print("Calculate mean \n", groupedByMarketCap['Amount Invested'].agg(np.mean))
print("Calculate size of groups \n", groupedByMarketCap.agg(np.size))
print("Calculate sum and mean \n", groupedByMarketCap['Amount Invested'].agg([np.sum, np.mean]))
currentSum = 57345
print("Calculate Total = ", portfolio['Amount Invested'].agg(np.sum), "Current total = ", currentSum)
total = portfolio['Amount Invested'].agg(np.sum)
profit = currentSum - total
profitPercentage = (profit/total) * 100
print("current profit =", profit, "Current Profit % = ", profitPercentage)

# Transformation
def z_score(x): return (x - x.mean()) / x.std()


print(groupedByMarketCap.transform(z_score))

# Filteration
# It will filter out the Groups that have less than 3 companies in that particular group
print("filter market cap = ", portfolio.groupby('MarketCap').filter(lambda x: len(x) >= 2))

# Merging and joining
left_df = pd.DataFrame({
    'id': [1, 2, 3, 4, 5],
    'Company': ['Infosys', 'SBI', 'Asian Paints', 'Maruti', 'Sun Pharma'],
    'Sector': ['IT', 'Banks', 'Paints and Varnishes', 'Auto', 'Pharma']})

right_df = pd.DataFrame(
    {'id': [1, 2, 3, 4, 5],
     'Company': ['NTPC', 'TCS', 'Lupin', 'ICICI', 'M&M'],
     'Sector': ['Power', 'IT', 'Pharma', 'Banks', 'Auto']})
print("Left \n", left_df)
print("Right \n", right_df)
print("Joining two dataframe on id \n ", pd.merge(left_df, right_df, on='id'))
print("Joining two dataframe on sector\n ", pd.merge(left_df, right_df, on='Sector'))
print("Joining two dataframe on sector and company\n ", pd.merge(left_df, right_df, on=['Sector', 'Company']))
print("Joining two dataframe on sector using left join\n ", pd.merge(left_df, right_df, on='Sector', how='left'))
print("Joining two dataframe on sector using right join\n ", pd.merge(left_df, right_df, on='Sector', how='right'))
print("Joining two dataframe on sector using  outer join\n ", pd.merge(left_df, right_df, on='Sector', how='outer'))
print("Joining two dataframe on sector using inner join\n ", pd.merge(left_df, right_df, on='Sector', how='inner'))

# Concatenation

print("Concat \n", pd.concat([left_df, right_df]))
print("Concat  with keys\n", pd.concat([left_df, right_df], keys=['x', 'y']))
print("Concat  with keys\n", pd.concat([left_df, right_df], keys=['x', 'y'], ignore_index=True))
print("Concat  with keys\n", pd.concat([left_df, right_df], keys=['x', 'y'], axis=1))

# Concatenating using append
print("Concat with append \n", left_df.append(right_df))
