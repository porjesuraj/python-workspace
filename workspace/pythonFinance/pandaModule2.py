import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import warnings
tcs = pd.read_csv("C:/Users/suraj/Documents/tcs.csv")
infy =  pd.read_csv("C:/Users/suraj/Documents/infy.csv")
tcs_new = tcs.drop(columns=["Series", "Prev Close", "Last Price", "Average Price", "Total Traded Quantity", "Turnover",
                            "No. of Trades", "Deliverable Qty", "% Dly Qt to Traded Qty"], axis=1)

tcs_new = tcs_new.rename(columns={"Open Price": "Open", "Close Price": "Close", "High Price": "High", "Low Price": "Low"})
plt.grid(True)
#tcs_new.plot()
#plt.show()



# DataFrame.cov()
# Covariance is a statistical measure that is used to determine
# the relationship between the movement of two asset prices.

print("Covariance = ", tcs["Close Price"].cov(infy["Close Price"]))

# DataFrame.corr()
# Correlation is a statistical measure that expresses the extent to which two variables are linearly related.
# A correlation of 0.53 indicates a quite strong correlation between these two stocks.
print("Correlation = ", tcs["Close Price"].corr(infy["Close Price"]))

# DataFrame.kurt()
# Kurtosis is a statistical measure that defines how heavily the tails of a distribution differ from the tails of a normal distribution.
# This method returns unbiased kurtosis over the requested data set
# using Fisher's definition of kurtosis (where kurtosis of normal distribution = 0).
print("Kurtosis = ", tcs["Close Price"].kurt())

# A positive kurtosis value indicates a leptokurtic distribution.
# Such a distribution can be described as having a flatter shape
# with fatter tails resulting in a greater chance of extreme positive or negative events.

# A negative kurtosis value indicates a platykurtic distribution.
# Such a distribution will have thinner tails, resulting in fewer extreme positive or negative events.


# DataFrame.skew()
# In statistics, skewness is a measure of the asymmetry of the distribution of a variable about its mean.
# This method unbiased skew over the requested data set.
print("Skewness = ", tcs["Close Price"].skew())

infy["Close Price"].skew()
sns.set(color_codes=True)
warnings.filterwarnings('ignore')
sns.distplot(infy["Close Price"])
plt.show()

sns.set(color_codes=True)

sns.distplot(tcs["Close Price"])
plt.show()
