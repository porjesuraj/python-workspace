# Single Line Comment
"""
Multi
Line
Comment
"""
print(1 + 2)

a = "Hello World"
print(a)
# List
b = ["Hello World", "Suraj"]

print(b)

b = b + [1, 2, 3, 4, 5]
print(b)
# Dictionary
c = {"color": "red"}
print(c["color"])
# Set
d = {1, 2, 2, 3, 4, 5, 6, 7}
print(d)

# If
if 1 == 1:
    print("Python if statement")

# Logical operator
print(True or False)
print(True and False)

# for

list1 = ["hello", "world"]
for item in list1:
    print("  " + item)

for i in range(0, 10):
    print(i)

# while

x = 10
while x > 0:
    print(x)
    x -= 1

# try and catch
try:
    print(list1[2])
except IndexError:
    print("index out of bound")


# function

def func():
    print("Hello World")


func()


# CLASS

class Person:
    def __init__(self):
        print("New Person")


p = Person()


# inheritance


class Suraj(Person):
    def __init__(self):
        super().__init__()
        print("Suraj")


s = Suraj()

# Modules

import math

print(math.pi)
