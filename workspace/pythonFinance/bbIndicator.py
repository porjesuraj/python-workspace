
def bollinger_bands(data, n):
    MA = data['Close'].rolling(window=n).mean()
    SD = data['Close'].rolling(window=n).std()

    data['Lower_BB'] = MA - (2 * SD)
    data['Upper_BB'] = MA + (2 * SD)

    return  data

import  pandas as pd
nifty = pd.read_csv('C:/Users/suraj/Documents/data.csv')
n = 21

nifty_bb = bollinger_bands(nifty, n)

print(nifty_bb.tail())

# Plotting the Bollinger Bands for 'Nifty' index
import matplotlib.pyplot as plt

plt.figure(figsize=(10,5))
plt.plot(nifty_bb.Close)
plt.plot(nifty_bb.Lower_BB)
plt.plot(nifty_bb.Upper_BB)
plt.grid(True)

plt.show()
from bokeh.plotting import figure, show, output_file

output_file("candlestick.html", title="bpcl 1 year candlestick.py ")

show(plt)