# Import pandas
import pandas as pd
import matplotlib.pyplot as plt

# Read data
df = pd.read_csv('C:/Users/suraj/Documents/BPCL2.csv', index_col=2)
df.head()
from bokeh.plotting import figure, show, output_file
# Indexing
import pandas as pd
from math import pi
w = 12 * 60 * 60 * 1000  # half day in ms
df.index = pd.to_datetime(df.index)

inc = df.Close > df.Open
dec = df.Open > df.Close
TOOLS = "pan,wheel_zoom,box_zoom,reset,save"
# Passing the arguments of our bokeh plot
p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=1000, title="BPCL Candlestick by Suraj")


# The orientation of major tick labels can be controlled with the major_label_orientation property.
# This property accepts the values "horizontal" or "vertical" or a floating point number that gives
# the angle (in radians) to rotate from the horizontal.
p.xaxis.major_label_orientation = pi / 4

# Alpha signifies the floating point between 0 (transparent) and 1 (opaque).
# The line specifies the alpha for the grid lines in the plot.
p.grid.grid_line_alpha = 0.3

# Configure and add segment glyphs to the figure
p.segment(df.index, df.High, df.index, df.Low, color="red")

# Adds vbar glyphs to the Figure
p.vbar(df.index[inc], w, df.Open[inc], df.Close[inc],
       fill_color="#1ED837", line_color="black")
p.vbar(df.index[dec], w, df.Open[dec], df.Close[dec],
       fill_color="#F2583E", line_color="black")

# Generates simple standalone HTML documents for Bokeh visualization
output_file("candlestick.html", title="bpcl 1 year candlestick.py ")

show(p)
